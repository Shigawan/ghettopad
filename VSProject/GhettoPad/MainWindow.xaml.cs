﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GhettoPad
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();
    }

    private void Handle_Exit()
    {
      Handle_SaveGhettoDoc();
      Application.Current.Shutdown();
    }

    private void Handle_SaveGhettoDoc()
    {
      var GhettoDirLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\GhettoPad";
      var GhettoDocLocation = GhettoDirLocation + "\\ghetto.txt";
      var currentText = txtBody.Text;

      if(!System.IO.File.Exists(GhettoDocLocation))
      {
        System.IO.Directory.CreateDirectory(GhettoDirLocation);
        using (var createdFile = System.IO.File.Create(GhettoDocLocation)) { }
      }

      System.IO.File.WriteAllText(GhettoDocLocation, currentText);
    }

    private void Handle_LoadGhettoDoc()
    {
      var GhettoDocLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\GhettoPad\\ghetto.txt";
      if(System.IO.File.Exists(GhettoDocLocation))
      {
        var loadedText = System.IO.File.ReadAllText(GhettoDocLocation);
        if (loadedText.Length > 0)
        {
          txtBody.Text = loadedText;
        }
      }
    }

    private void Window_KeyDown(object sender, KeyEventArgs e)
    {
      if(e.Key == Key.Escape)
      {
        Handle_Exit();
      }
    }

    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      Handle_LoadGhettoDoc();
    }
  }
}
