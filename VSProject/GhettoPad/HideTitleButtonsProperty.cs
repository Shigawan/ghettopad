﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Interop;

namespace GhettoPad
{
  public class HideTitleButtonsPropertyBehavior
  {
    private static readonly Type OwnerType = typeof(HideTitleButtonsPropertyBehavior);

    #region HideTitleButtons (attached property)

    public static readonly DependencyProperty HideTitleButtonsProperty =
        DependencyProperty.RegisterAttached(
            "HideTitleButtons",
            typeof(bool),
            OwnerType,
            new FrameworkPropertyMetadata(false, new PropertyChangedCallback(HideTitleButtonsChangedCallback)));

    [AttachedPropertyBrowsableForType(typeof(Window))]
    public static bool GetHideTitleButtons(Window obj)
    {
      return (bool)obj.GetValue(HideTitleButtonsProperty);
    }

    [AttachedPropertyBrowsableForType(typeof(Window))]
    public static void SetHideTitleButtons(Window obj, bool value)
    {
      obj.SetValue(HideTitleButtonsProperty, value);
    }

    private static void HideTitleButtonsChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var window = d as Window;
      if (window == null) return;

      var hideTitleButtons = (bool)e.NewValue;
      if (hideTitleButtons && !GetIsHiddenTitleButtons(window))
      {
        if (!window.IsLoaded)
        {
          window.Loaded += HideWhenLoadedDelegate;
        }
        else {
          HideTitleButtons(window);
        }
        SetIsHiddenTitleButtons(window, true);
      }
      else if (!hideTitleButtons && GetIsHiddenTitleButtons(window))
      {
        if (!window.IsLoaded)
        {
          window.Loaded += ShowWhenLoadedDelegate;
        }
        else {
          ShowCloseButton(window);
        }
        SetIsHiddenTitleButtons(window, false);
      }
    }

    #region Win32 imports

    private const int GWL_STYLE = -16;
    private const int WS_SYSMENU = 0x80000;
    [DllImport("user32.dll", SetLastError = true)]
    private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    #endregion

    private static readonly RoutedEventHandler HideWhenLoadedDelegate = (sender, args) => {
      if (sender is Window == false) return;
      var w = (Window)sender;
      HideTitleButtons(w);
      w.Loaded -= HideWhenLoadedDelegate;
    };

    private static readonly RoutedEventHandler ShowWhenLoadedDelegate = (sender, args) => {
      if (sender is Window == false) return;
      var w = (Window)sender;
      ShowCloseButton(w);
      w.Loaded -= ShowWhenLoadedDelegate;
    };

    private static void HideTitleButtons(Window w)
    {
      var hwnd = new WindowInteropHelper(w).Handle;
      SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
    }

    private static void ShowCloseButton(Window w)
    {
      var hwnd = new WindowInteropHelper(w).Handle;
      SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) | WS_SYSMENU);
    }

    #endregion

    #region IsHiddenTitleButtons (readonly attached property)

    private static readonly DependencyPropertyKey IsHiddenTitleButtonsKey =
        DependencyProperty.RegisterAttachedReadOnly(
            "IsHiddenTitleButtons",
            typeof(bool),
            OwnerType,
            new FrameworkPropertyMetadata(false));

    public static readonly DependencyProperty IsHiddenTitleButtonsProperty =
        IsHiddenTitleButtonsKey.DependencyProperty;

    [AttachedPropertyBrowsableForType(typeof(Window))]
    public static bool GetIsHiddenTitleButtons(Window obj)
    {
      return (bool)obj.GetValue(IsHiddenTitleButtonsProperty);
    }

    private static void SetIsHiddenTitleButtons(Window obj, bool value)
    {
      obj.SetValue(IsHiddenTitleButtonsKey, value);
    }

    #endregion
  }
}
